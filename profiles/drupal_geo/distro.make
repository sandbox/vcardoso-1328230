; Use this file to build a full distribution including Drupal core and the
; Drupal Geo install profile using the following command:
;
; drush make distro.make <target directory>

api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = "7"
;projects[feeds][patch][1213472] = http://drupal.org/files/issues/FeedsConfigurable-patch.diff
;projects[drupal_geo][type] = profile
;projects[drupal_geo][version] = 1.x-dev
;projects[drupal_geo][download][type] = git
;projects[drupal_geo][download][url] = http://git.drupal.org/project/drupal_geo.git
;projects[drupal_geo][download][branch] = 7.x-1.x
