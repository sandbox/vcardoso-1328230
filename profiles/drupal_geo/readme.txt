Installation
============
1) Place this folder in the profiles directory.
2) Place the required modules in a subdirectory of this folder entitled "modules".
3) When prompted for install profile, choose "Drupal Geo".

Credits
=======
Victor Cardoso (vcardoso)
