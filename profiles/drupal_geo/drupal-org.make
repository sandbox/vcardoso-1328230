; Drupal.org release file.
core = 7.9
api = 2

; General purpose modules
projects[admin_menu] = 3.0-rc1
projects[backup_migrate] = 2.2
projects[ctools] = 1.x-dev
projects[feeds] = 2.x-dev
projects[job_scheduler] = 2.0-alpha2
projects[libraries] = 2.0-alpha1
projects[module_filter] = 1.6
projects[pathauto] = 1.0
projects[token] = 1.0-beta7
projects[views] = 3.x-dev
projects[views_bulk_operations] = 7.x-3.0-beta3

; Geo related modules
projects[addressfield] = 1.0-beta2
projects[geocoder] = 1.x-dev
projects[geofield] = 1.0-alpha5
projects[geolocation] = 1.0-beta2
projects[geonames] = 1.0
projects[gmap] = 1.x-dev
projects[gmap_addons] = 1.x-dev
projects[location] = 3.x-dev
projects[location_feeds] = 1.4
projects[mapbox] = 2.0-alpha1
projects[openlayers] = 2.0-alpha2
projects[openlayers_kml_layer] = 1.0-beta1
